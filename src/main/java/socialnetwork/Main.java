package socialnetwork;

import jdk.vm.ci.meta.Local;
import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.NuExistaInRepoException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.FriendRequestDB;
import socialnetwork.repository.database.MessageDB;
import socialnetwork.repository.database.PrietenieDB;
import socialnetwork.repository.database.UtilizatorDB;
//import socialnetwork.repository.file.FriendRequestFile;
//import socialnetwork.repository.file.MessageFile;
//import socialnetwork.repository.file.PrietenieFile;
//import socialnetwork.repository.file.UtilizatorFile;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieException;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.ui.ConsoleUI;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/*
drop table utilizatori;drop table prietenii;drop table friendrequests;drop table messages;
create table utilizatori(id BIGSERIAL PRIMARY KEY,firstname varchar(50),lastname varchar(50));create table prietenii(id varchar(50),date varchar(50));create table friendrequests (id varchar(100) primary key,status varchar(30));
create table messages (id BIGSERIAL PRIMARY KEY,from_id BIGINT,to_ids varchar(300),message varchar(300),data varchar(100),reply varchar(300));
*/
public class Main {
//    private static void teste(Repository<Long, Utilizator> userFileRepository, Repository<Tuple<Long, Long>, Prietenie> prietenieFileRepository, UtilizatorService srv, PrietenieService prSrv) {
//        System.out.println("Lista initiala de utilizatori:\n" + srv.getAll());
//        System.out.println("Lista initiala de prietenii:\n" + prSrv.getAll());
//        srv.addUtilizator("Nume", "Nou");
//        System.out.println("Lista de utilizatori dupa adaugarea unuia nou:\n" + srv.getAll());
//        Long lastID = Long.parseLong(userFileRepository.getLastID());
//        try {
//            prSrv.addPrietenie(1L, lastID);
//            System.out.println("Lista de prietenii dupa adaugarea uneia noi:\n" + prSrv.getAll());
//            prSrv.addPrietenie(3L, lastID);
//            System.out.println("Lista de prietenii dupa adaugarea alteia noi:\n" + prSrv.getAll());
//        } catch (PrietenieException prietenieException) {
//            System.out.println(prietenieException.getMessage());
//        }
//        System.out.println("Cea mai sociabila comunitate:\n" + prSrv.getMostSociableCommunity());
//
//        try {
//            prSrv.deleteUtilizatorAndFriendships(lastID);
//        } catch (NuExistaInRepoException nuExistaInRepoException) {
//            System.out.println(nuExistaInRepoException.getMessage());
//        }
//        System.out.println("Lista de utilizatori dupa stergerea unuia:\n" + srv.getAll());
//        System.out.println("Lista de prietenii dupa stergerea unui utilizator:\n" + prSrv.getAll());
//
//        System.out.println("Incercam adaugarea unei prietenii invalide");
//        try {
//            prSrv.addPrietenie(lastID + 1, lastID + 3);
//        } catch (PrietenieException prietenieException) {
//            System.out.println(prietenieException.getMessage());
//        }
//
//        System.out.println("Incercam adaugarea unui utilizator fara nume si prenume");
//        try {
//            srv.addUtilizator("", "");
//        } catch (ValidationException validationException) {
//            System.out.println(validationException.getMessage());
//        }
//
//    }
    private static void testeDB() {
        String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
        String username = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username");
        String pass = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");
        String columnsUtilizator = "(firstname, lastname)";
        String columnsPrietenii = "(id1,id2,date)";
        String columnsFrqs = "(id1,id2,status)";
        String columnsMsgs = "(from_id,to_ids,message,data,reply)";
        String utiSerialSeqName="utilizatori_id_seq";
        String msgsSerialSrqName="messages_id_seq";
        Repository<Long, Utilizator> userDBRepo = new UtilizatorDB(url, username, pass, new UtilizatorValidator(), "utilizatori", columnsUtilizator,utiSerialSeqName);
        Repository<Tuple<Long, Long>, Prietenie> prDBRepo = new PrietenieDB(url, username, pass, new PrietenieValidator(), "prietenii", columnsPrietenii);
        Repository<Tuple<Long, Long>, FriendRequest> frqDBRepo = new FriendRequestDB(url, username, pass, new FriendRequestValidator(), "friendrequests", columnsFrqs);
        Repository<Long, Message> msgsDBRepo = new MessageDB(url, username, pass, new MessageValidator(), "messages", columnsMsgs,msgsSerialSrqName,userDBRepo);

        UtilizatorService srv = new UtilizatorService(userDBRepo);
        PrietenieService prSrv = new PrietenieService(prDBRepo, userDBRepo, frqDBRepo);
        MessageService msgSrv = new MessageService(userDBRepo, msgsDBRepo, prDBRepo, frqDBRepo);

        LocalDateTime craciun2k19= LocalDateTime.of(2019, Month.DECEMBER,24,12,24,36);
//
//        Prietenie p1=new Prietenie(9l,5l);
//        p1.setDate(craciun2k19);
//        Prietenie p2=new Prietenie(9l,6l);
//        p2.setDate(craciun2k19);
//        Prietenie p3=new Prietenie(9l,7l);
//        p3.setDate(craciun2k19);
//        Prietenie p4=new Prietenie(9l,8l);
//        p4.setDate(craciun2k19);
//        prDBRepo.save(p1);prDBRepo.save(p2);prDBRepo.save(p3);prDBRepo.save(p4);
//        System.out.println(craciun2k19);

//        //adaug 4 useri
//        srv.addUtilizator("Tester1","BugMaker");
//        String idS=userDBRepo.getLastID();
//        Long id1=Long.parseLong(idS);
//        srv.addUtilizator("Tester2","BugFinder");
//        Long id2=Long.parseLong(userDBRepo.getLastID());
//        srv.addUtilizator("Tester3","BugSolutionProposer");
//        Long id3=Long.parseLong(userDBRepo.getLastID());
//        srv.addUtilizator("Tester4","BugSolver");
//        Long id4=Long.parseLong(userDBRepo.getLastID());
//        System.out.println(userDBRepo.findAll());
//        srv.addUtilizator("Branza","William");
//        srv.addUtilizator("Pufulete","Rudolf");
//        srv.addUtilizator("Cel-Mircea","Stefan");
//        srv.addUtilizator("Puscas","Marin");
//        srv.addUtilizator("Mos","Craciun");
//        //stabilesc prietenii intre 1 si 2 si intre 1 si 3
//        prSrv.sendFriendRequest(id1,id2);
//        prSrv.sendFriendRequest(id1,id3);
//        //incerc sa trimit un mesaj intre doi useri care nu si-au acceptat cererea de prietenie
//        try{
//            ArrayList<Utilizator> to=new ArrayList<>();
//            to.add(userDBRepo.findOne(id2));
//            msgSrv.sendMessage(id1,to,"testMsg");
//            System.out.println("S-a trimis un mesaj invalid => assertion failed");
//        }catch(PrietenieException pre){
//            System.out.println(pre.getMessage());
//        }
//        try {
//            prSrv.acceptFriendRequest(id2,id1);
//            System.out.println("Am acceptat cererea de prietenie dintre id1 si id2");
//        } catch (PrietenieException prietenieException) {
//            System.out.println(prietenieException.getMessage());
//        }
//        //incerc sa trimit mesaj intre prieteni
//        try {
//            ArrayList<Utilizator> to=new ArrayList<>();
//            to.add(userDBRepo.findOne(id2));
//            msgSrv.sendMessage(id1,to,"testMsg");
//            Long msgId1=Long.parseLong(msgsDBRepo.getLastID());
//            System.out.println("Am trimis mesajul: "+msgsDBRepo.findOne(msgId1));
//            msgSrv.replyTo(id2,msgId1,"raspuns to testMsg");
//        } catch (PrietenieException prietenieException) {
//            prietenieException.printStackTrace();
//        }catch (NuExistaInRepoException neire){
//            neire.printStackTrace();
//        }


    }
    public static void DBRepo(){
        final boolean testMode=false;
        if(testMode)
            testeDB();
        else {
            //select * from utilizatori;select * from prietenii;select * from friendrequests;select * from messages;
            String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
            String username = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username");
            String pass = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");
            String columnsUtilizator = "(firstname, lastname)";
            String columnsPrietenii = "(id1,id2,date)";
            String columnsFrqs = "(id1,id2,status)";
            String columnsMsgs = "(from_id,to_ids,message,data,reply)";
            String utiSerialSeqName="utilizatori_id_seq";
            String msgsSerialSrqName="messages_id_seq";
            Repository<Long, Utilizator> userDBRepo = new UtilizatorDB(url, username, pass, new UtilizatorValidator(), "utilizatori", columnsUtilizator,utiSerialSeqName);
            Repository<Tuple<Long, Long>, Prietenie> prDBRepo = new PrietenieDB(url, username, pass, new PrietenieValidator(), "prietenii", columnsPrietenii);
            Repository<Tuple<Long, Long>, FriendRequest> frqDBRepo = new FriendRequestDB(url, username, pass, new FriendRequestValidator(), "friendrequests", columnsFrqs);
            Repository<Long, Message> msgsDBRepo = new MessageDB(url, username, pass, new MessageValidator(), "messages", columnsMsgs,msgsSerialSrqName,userDBRepo);

            UtilizatorService srv = new UtilizatorService(userDBRepo);
            PrietenieService prSrv = new PrietenieService(prDBRepo, userDBRepo, frqDBRepo);
            MessageService msgSrv = new MessageService(userDBRepo, msgsDBRepo, prDBRepo, frqDBRepo);
            ConsoleUI ui = new ConsoleUI(prSrv, srv, msgSrv);
            try {
                ui.runUI();
            } catch (Exception exception) {
                exception.printStackTrace();

            }
        }
    }



//    public static void fileRepo(){
////        final boolean testMode = true;
////
////        String fileName = ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.users");
////        //String prFileName = ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.prietenii");
////        //String fileName="data/users.csv";
////        String prFileName = "data/prietenii.csv";
////        String msgFileName= "data/mesaje.csv";
////        String frqFileName= "data/friendRequests.csv";
////        Repository<Long, Utilizator> userFileRepository = new UtilizatorFile(fileName
////                , new UtilizatorValidator());
////
////        Repository<Tuple<Long, Long>, Prietenie> prietenieFileRepository = new PrietenieFile(prFileName, new PrietenieValidator());
////        Repository<Long, Message> messageFileRepository=new MessageFile(msgFileName,new MessageValidator());
////        Repository<Tuple<Long,Long>, FriendRequest> frqRepo=new FriendRequestFile(frqFileName,new FriendRequestValidator());
////        UtilizatorService srv = new UtilizatorService(userFileRepository);
////        PrietenieService prSrv = new PrietenieService(prietenieFileRepository, userFileRepository,frqRepo);
////        MessageService msgSrv=new MessageService(userFileRepository,messageFileRepository,prietenieFileRepository,frqRepo);
////        if (testMode)
////            teste(userFileRepository, prietenieFileRepository, srv, prSrv);
////        else {
////            ConsoleUI ui = new ConsoleUI(prSrv, srv,msgSrv);
////            try {
////                ui.runUI();
////            } catch (Exception exception) {
////                exception.printStackTrace();
////
////            }
////        }
//    }

    public static void main(String[] args) {
        final boolean db=true;
        if(!db){
            //fileRepo();
        }
        else{
            DBRepo();
        }
    }
}


