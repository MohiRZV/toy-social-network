package socialnetwork.ui;

import socialnetwork.domain.Message;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.NuExistaInRepoException;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieException;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ConsoleUI {
    private PrietenieService psrv;
    private UtilizatorService usrv;
    private MessageService msrv;
    private String comenziDisponibile="Comenzile disponibile sunt:\n\taddu - adauga utilizator\n\taddp - adauga prietenie\n\t"+
            "delu - sterge utilizator\n\tdelp - sterge prietenie\n\tcc - afiseaza componentele conexe din retea\n\t"+
            "fall - afiseaza toti prietenii unui utilizator\n\tfrbm - afiseaza\n\t"+
            "send - trimite un mesaj\n\treply - raspunde la un mesaj\n\treplyall - raspunde la toti participantii\n\t"+
            "convos - afiseaza conversatiile a doi useri in ordine cronologica\n\t"+
            "sfrq - trimite friend request\n\tafrq - accepta friend request\n\trfrq - refuza friend request";
//TODO Reply all
    public ConsoleUI(PrietenieService psrv, UtilizatorService usrv,MessageService msrv) {
        this.psrv = psrv;
        this.usrv = usrv;
        this.msrv=msrv;
    }

    void addu()throws IOException{
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Introduceti numele utilizatorului:");
        String nume=bufferedReader.readLine();
        System.out.println("Introduceti prenumele utilizatorului:");
        String prenume=bufferedReader.readLine();
        try {
            usrv.addUtilizator(nume, prenume);
        }catch(ValidationException validationException){
            System.out.println(validationException.getMessage());
        }
    }

    public void runUI()throws IOException {
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
        System.out.println(comenziDisponibile);
        System.out.println("Introduceti comanda dorita:");
        String cmd=bufferedReader.readLine();
        while(!cmd.equals("stop")){
                if (cmd.equals("addu"))
                    addu();
                else if (cmd.equals("addp"))
                    addp();
                else if (cmd.equals("delu"))
                    delu();
                else if (cmd.equals("delp"))
                    delp();
                else if (cmd.equals("cc"))
                    cc();
                else if (cmd.equals("msc"))
                    msc();
                else if (cmd.equals("fall"))
                    fall();
                else if (cmd.equals("frbm"))
                    frbm();
                else if (cmd.equals("send"))
                    send();
                else if (cmd.equals("reply"))
                    reply();
                else if (cmd.equals("convos"))
                    convos();
                else if (cmd.equals("sfrq"))
                    sfrq();
                else if (cmd.equals("afrq"))
                    afrq();
                else if (cmd.equals("rfrq"))
                    rfrq();
                else if (cmd.equals("replyall"))
                    replyall();
                else
                    System.out.println("Comanda introdusa este invalida!");
            System.out.println("Introduceti comanda dorita:");
            cmd=bufferedReader.readLine();
        }
    }

    private void rfrq() {
        try{
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Introduceti id-urile celor doi utilizatori");
            String str=bufferedReader.readLine();
            String[] ids=str.split(" ");
            if(ids.length!=2){
                System.out.println("Trebuiesc introduse cele 2 iduri separate prin spatiu!");
                return;
            }
            psrv.rejectFriendRequest(Long.parseLong(ids[0]),Long.parseLong(ids[1]));
        }catch (Exception Exception){
            System.out.println(Exception);
        }

    }

    private void afrq() {
        try{
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Introduceti id-urile celor doi utilizatori");
            String str=bufferedReader.readLine();
            String[] ids=str.split(" ");
            if(ids.length!=2){
                System.out.println("Trebuiesc introduse cele 2 iduri separate prin spatiu!");
                return;
            }
            psrv.acceptFriendRequest(Long.parseLong(ids[0]),Long.parseLong(ids[1]));
        }catch (Exception Exception){
            System.out.println(Exception);
        }
    }

    private void sfrq() {
        try{
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Introduceti id-urile celor doi utilizatori");
            String str=bufferedReader.readLine();
            String[] ids=str.split(" ");
            if(ids.length!=2){
                System.out.println("Trebuiesc introduse cele 2 iduri separate prin spatiu!");
                return;
            }
            psrv.sendFriendRequest(Long.parseLong(ids[0]),Long.parseLong(ids[1]));
        }catch (Exception Exception){
            System.out.println(Exception);
        }
    }

    private void convos(){//TODO move to srv
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Introduceti id-urile celor doi utilizatori");
            String str = bufferedReader.readLine();
            String[] ids = str.split(" ");
            if(ids.length!=2){
                System.out.println("Trebuiesc introduse cele 2 iduri separate prin spatiu!");
                return;
            }
            List<Message> cnvs = msrv.sortedConvos(Long.parseLong(ids[0]), Long.parseLong(ids[1]));
            cnvs.forEach(x -> System.out.println(x));
        }catch(IOException ioException){
            ioException.printStackTrace();
        }
    }

    private void reply(){
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Introduceti id-ul utilizatorului care trimite mesajul");
            String str = bufferedReader.readLine();
            Long fromID = Long.parseLong(str);
            System.out.println("Introduceti id-ul mesajului la care se raspunde");
            str = bufferedReader.readLine();
            Long to = Long.parseLong(str);
            System.out.println("Introduceti mesajul");
            String mesaj = bufferedReader.readLine();
            msrv.replyTo(fromID, to, mesaj);
        }catch(IOException ioException){
            ioException.printStackTrace();
        }catch (PrietenieException prietenieException){
            System.out.println(prietenieException.getMessage());
        }
        catch (NuExistaInRepoException nuExistaInRepoException){
            System.out.println(nuExistaInRepoException.getMessage());
        }
    }
    private void replyall(){
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Introduceti id-ul utilizatorului care trimite mesajul");
            String str = bufferedReader.readLine();
            Long fromID = Long.parseLong(str);
            System.out.println("Introduceti id-ul mesajului la care se raspunde");
            str = bufferedReader.readLine();
            Long to = Long.parseLong(str);
            System.out.println("Introduceti mesajul");
            String mesaj = bufferedReader.readLine();
            msrv.replyAll(fromID, to, mesaj);
        }catch(IOException ioException){
            ioException.printStackTrace();
        }catch (PrietenieException prietenieException){
            System.out.println(prietenieException.getMessage());
        }
        catch (NuExistaInRepoException nuExistaInRepoException){
            System.out.println(nuExistaInRepoException.getMessage());
        }
    }
    private void send(){
        try{
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Introduceti id-ul utilizatorului care trimite mesajul");
        String str=bufferedReader.readLine();
        Long fromID=Long.parseLong(str);
        System.out.println("Introduceti id-ul utilizatorilor la care se trimite mesajul");
        str=bufferedReader.readLine();
        String[] toIds=str.split(" ");
        ArrayList<Utilizator> to=new ArrayList<>();
        for(String i:toIds)
            to.add(usrv.getOne(Long.parseLong(i)));
        System.out.println("Introduceti mesajul");
        String mesaj=bufferedReader.readLine();
        msrv.sendMessage(fromID, to, mesaj);
        }catch(IOException ioException){
            System.out.println(ioException.getMessage());
        }catch(PrietenieException prietenieException){
            System.out.println(prietenieException.getMessage());
        }

    }

    private void frbm(){
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Introduceti id-ul utilizatorului si luna dorita");
            String str = bufferedReader.readLine();
            String parts[] = str.split(" ");
            if(parts.length!=2){
                System.out.println("Trebuiesc introduse cele doua valori una dupa alta, separate prin spatiu!");
                return;
            }
            Long uID = Long.parseLong(parts[0]);
            Integer month = Integer.parseInt(parts[1]);
            Utilizator u = usrv.getOne(uID);
            if(u==null){
                System.out.println("Utilizatorul cautat nu exista!");
                return;
            }
            if(month<1 || month>12){
                System.out.println("Luna nu este valida!");
                return;
            }
            psrv.loadOnesFriends(u);
            Optional<String> op = u.getFriends().
                    stream().
                    filter(x -> {
                        Tuple<Long, Long> pid = new Tuple<Long, Long>(uID, x.getId());
                        LocalDateTime data = psrv.getOne(pid).getDate();
                        return data.getMonth().getValue() == month;
                    }).
                    map(x -> {
                        Tuple<Long, Long> pid = new Tuple<Long, Long>(uID, x.getId());
                        return x.getFirstName() + " | " + x.getLastName() + " | " + psrv.getOne(pid).getDate();
                    })
                    .reduce((x, y) -> x.concat("\n").concat(y));
            op.ifPresent(x -> System.out.println(x));
        }catch (IOException ioException){
            System.out.println(ioException.getMessage());
        }

    }
    private void fall(){//TODO move to srv
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Introduceti id-ul utilizatorului:");
            String id = bufferedReader.readLine();
            Long uID = Long.parseLong(id);
            Utilizator u = usrv.getOne(uID);
            if(u==null) {
                System.out.println("Utilizatorul dorit nu exista!");
                return;
            }
            psrv.loadOnesFriends(u);
            Optional<String> op = u.getFriends().
                    stream().
                    map(x -> {
                        Tuple<Long, Long> pid = new Tuple<Long, Long>(uID, x.getId());
                        return x.getFirstName() + " | " + x.getLastName() + " | " + psrv.getOne(pid).getDate();
                    })
                    .reduce((x, y) -> x.concat("\n" + y));
            op.ifPresent(x -> System.out.println(x));
        }catch (IOException ioException){
            System.out.println(ioException.getMessage());
        }

    }
    private void cc() {
        List<List<Integer>> comp = psrv.getComponenteConexe();
        System.out.println("Componentele conexe ale retelei sunt " + comp + "\nsi sunt in numar de " + comp.size());
    }
    private void msc(){
        System.out.println("Cea mai sociabila comunitate este cea formata din utilizatorii: "+psrv.getMostSociableCommunity());
    }
    private void addp() throws IOException{//legacy
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Introduceti id-urile celor doi prieteni");
        String idsS=bufferedReader.readLine();
        String[] ids=idsS.split(" ");
        try{
            psrv.addPrietenie(Long.parseLong(ids[0]),Long.parseLong(ids[1]));
        }catch(ValidationException validationException){
            System.out.println(validationException.getMessage());
        }catch (PrietenieException prietenieException){
            System.out.println(prietenieException.getMessage());
        }
    }
    private void delu(){
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Introduceti id-ul utilizatorului pe care doriti sa il stergeti:");
            String id = bufferedReader.readLine();
            psrv.deleteUtilizatorAndFriendships(Long.parseLong(id));
        }catch (IOException ioException){
            System.out.println(ioException.getMessage());
        }catch (NuExistaInRepoException neire){
            System.out.println(neire.getMessage());
        }
    }
    private void delp(){
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Introduceti id-urile componentilor prieteniei pe care doriti sa o stergeti:");
            String id = bufferedReader.readLine();
            String[] ids = id.split(" ");
            if(ids.length!=2)
            {
                System.out.println("Introduceti cele doua id-uri separate prin spatiu!");
                return;
            }
            psrv.deletePrietenie(new Tuple<Long, Long>(Long.parseLong(ids[0]), Long.parseLong(ids[1])));
        }catch (IOException ioException){
            System.out.println(ioException.getMessage());
        }
        catch (NuExistaInRepoException neire){
            System.out.println(neire.getMessage());
        }
    }
}
