package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.NuExistaInRepoException;
import socialnetwork.repository.Repository;
import sun.nio.ch.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MessageService {
    private Repository<Long,Utilizator> uRepo;
    private Repository<Long,Message> mRepo;
    private Repository<Tuple<Long,Long>, Prietenie> pRepo;
    private Repository<Tuple<Long,Long>, FriendRequest> frqRepo;
    private Long lastID;
    public MessageService(Repository<Long,Utilizator> uRepo, Repository<Long,Message> mRepo,Repository<Tuple<Long,Long>, Prietenie> pRepo,Repository<Tuple<Long,Long>, FriendRequest> frqRepo) {
        this.uRepo = uRepo;
        this.mRepo = mRepo;
        this.pRepo=pRepo;
        this.frqRepo=frqRepo;
        //lastID=Long.parseLong(mRepo.getLastID());
    }

    public void sendMessage(Long from,ArrayList<Utilizator> to,String message)throws PrietenieException{
        //for(Long i:to)
            //if(pRepo.findOne(new Tuple<>(from,i))==null)
            for(Utilizator utilizator:to)
                if(pRepo.findOne(new Tuple<>(from,utilizator.getId()))==null)
                {
                    FriendRequest frq=frqRepo.findOne(new Tuple<Long,Long>(from,utilizator.getId()));
                    String stat="";
                    if(frq==null)
                        stat="\nTrimiteti-i o cerere!";
                    else if(frq.getStatus().name().equals("REJECTED"))
                        stat="\nVi s-a respins cererea de prietenie!";
                    else if(frq.getStatus().toString().equals("PENDING"))
                        stat="\nAsteptati pana vi se accepta cererea de pritenie!";

                    throw new PrietenieException("Utilizatorii "+from.toString()+" si "+utilizator.getId().toString()+" nu sunt prieteni!"+stat);
                }
        Message msg=new Message(0l,from,to,message);
        mRepo.save(msg);
    }

    /**
     * raspunde la un mesaj
     * @param from - id utilizator
     * @param to - id mesaj
     * @param message
     * @throws PrietenieException
     * @throws NuExistaInRepoException
     *///TODO check if the user got the message he wants to reply to
    public void replyTo(Long from,Long to,String message)throws PrietenieException,NuExistaInRepoException{
        Message repMsg=mRepo.findOne(to);
        if(uRepo.findOne(from)==null)
            throw new NuExistaInRepoException("Utilizatorul care trimite mesajul e hacerman!");
        if(repMsg==null)
            throw new NuExistaInRepoException("Mesajul la care doriti sa dati reply nu exista!");
        if(pRepo.findOne(new Tuple<>(from,repMsg.getFrom()))==null) {
            FriendRequest frq=frqRepo.findOne(new Tuple<>(from,to));
            String stat="";
            if(frq==null)
                stat="\nTrimiteti-i o cerere!";
            else if(frq.getStatus().equals("rejected"))
                stat="\nVi s-a respins cererea de prietenie!";
            else if(frq.getStatus().equals("pending"))
                stat="\nAsteptati pana vi se accepta cererea de pritenie!";

            throw new PrietenieException("Utilizatorii "+from.toString()+" si "+to.toString()+" nu sunt prieteni!"+stat);
        }
        List<Utilizator> t=new ArrayList<>();
        t.add(uRepo.findOne(repMsg.getFrom()));


        Message msg=new Message(0l,from,t,message);
        mRepo.save(msg);
        Long id=Long.parseLong(mRepo.getLastID());
        Message m=mRepo.findOne(to);
        m.addReply(mRepo.findOne(id));
        mRepo.update(m);
//        List<Long> t=new ArrayList<>();
//        t.add(to);
//        Message msg=new Message(0l,from,t,message);
//        mRepo.save(msg);
//        Long id=Long.parseLong(mRepo.getLastID());
//        Message m=mRepo.findOne(to);
//        m.addReply(id);
//        mRepo.update(m);
    }
    public void replyAll(Long from,Long to,String message)throws PrietenieException,NuExistaInRepoException{
        Message repMsg=mRepo.findOne(to);
        List<Utilizator> t=new ArrayList<>();
        List<Utilizator> re=repMsg.getTo();
        for(Utilizator uti:re)//adaug utilizatorii din to-ul mesajului (diferiti de cel curent) in t
            if(uti.getId()!=from)
                t.add(uti);
        if(uRepo.findOne(from)==null)
            throw new NuExistaInRepoException("Utilizatorul care trimite mesajul e hacerman!");
        if(repMsg==null)
            throw new NuExistaInRepoException("Mesajul la care doriti sa dati reply nu exista!");

        if(repMsg.getFrom()!=from)
            t.add(uRepo.findOne(repMsg.getFrom()));

        Message msg=new Message(0l,from,t,message);
        mRepo.save(msg);
        Long id=Long.parseLong(mRepo.getLastID());
        Message m = mRepo.findOne(to);
        Message current=mRepo.findOne(id);
        for(Message rmsg:m.getReply()) {//actualizez lista de reply a mesajelor

            rmsg.addReply(current);
            mRepo.update(rmsg);
        }
    }
    private List<Message> getConvos(Long id1,Long id2){
        List<Message> msgs=new ArrayList<>();
        Utilizator u1=uRepo.findOne(id1);
        Utilizator u2=uRepo.findOne(id2);
        for(Message msg:mRepo.findAll()){
           boolean hasId1=false,hasId2=false;
           if(msg.getFrom()==id1)
           {
               hasId1=true;
//               for(Long idm:msg.getTo())
//                   if(idm==id2)
//                       hasId2=true;
               for(Utilizator utilizator:msg.getTo())
                   if(utilizator.getId()==id2)
                       hasId2=true;
           }
           if(msg.getFrom()==id2)
           {
               hasId2=true;
//               for(Long idm:msg.getTo())
//                   if(idm==id1)
//                       hasId1=true;
               for(Utilizator utilizator:msg.getTo())
                   if(utilizator.getId()==id1)
                       hasId1=true;
           }
           if(hasId1 && hasId2)
               msgs.add(msg);

        }
        return msgs;
    }

    public List<Message> sortedConvos(Long id1,Long id2){
        List<Message> cnvs=getConvos(id1,id2);
        Collections.sort(cnvs,(x,y)->{
            return x.getData().compareTo(y.getData());
        });
        return cnvs;
    }

    public Iterable<Message> findAll(){
        return mRepo.findAll();
    }
}
