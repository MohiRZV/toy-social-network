package socialnetwork.service;

import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;

public class UtilizatorService  {
    private Repository<Long, Utilizator> repo;

    private Long lastID;
    public UtilizatorService(Repository<Long, Utilizator> repo) {
        this.repo = repo;
        //lastID=Long.parseLong(repo.getLastID());
    }

    /**
     * Creates and stores an Utilizator entity
     * @param nume
     * @param prenume
     * @return null if the storing was succesfull,
     *                                      the entity otherwise
     */
    public Utilizator addUtilizator(String nume,String prenume) {
        Utilizator u=new Utilizator(nume,prenume);
        Utilizator ret = repo.save(u);
        return ret;
    }

    /**
     * Deletes the Utilizator entity with the given id
     * @param id
     * @return the entity if the deletion was succesfull,
     *                                  null otherwise
     */
    public Utilizator deleteUtilizator(Long id){
        Utilizator u = repo.delete(id);
        return u;
    }

    public Utilizator getOne(Long id){
        return repo.findOne(id);
    }

    public Iterable<Utilizator> getAll(){
        return repo.findAll();
    }


    ///TO DO: add other methods
}
