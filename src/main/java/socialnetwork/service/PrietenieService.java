package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.NuExistaInRepoException;
import socialnetwork.repository.Repository;
import sun.nio.ch.Util;

import java.util.*;

public class PrietenieService  {
    private Repository<Tuple<Long,Long>, Prietenie> repo;
    private Repository<Long, Utilizator> utiRepo;
    private Repository<Tuple<Long,Long>, FriendRequest> frqRepo;

    public PrietenieService(Repository<Tuple<Long,Long>, Prietenie> repo,Repository<Long, Utilizator> utiRepo,Repository<Tuple<Long,Long>, FriendRequest> frqRepo) {
        this.repo = repo;
        this.utiRepo=utiRepo;
        this.frqRepo=frqRepo;
        //loadFriendLists();
    }

    /**
     * Loads the friend lists of all users
     */
    public void loadFriendLists(){
        repo.findAll().forEach(x->{
            Utilizator u1= utiRepo.findOne(x.getId().getLeft());
            Utilizator u2= utiRepo.findOne(x.getId().getRight());
            u1.addFriend(u2);
            u2.addFriend(u1);

        });
    }
    public void loadOnesFriends(Utilizator u){
        repo.findAll().forEach(x->{
            if(x.getId().getLeft()==u.getId()){
                u.addFriend(utiRepo.findOne(x.getId().getRight()));
            }else if(x.getId().getRight()==u.getId()){
                u.addFriend(utiRepo.findOne(x.getId().getLeft()));
            }

        });
    }

    /**
     * send a friend request from user with id1 to user with id2
     * @param id1 - Long
     * @param id2 - Long
     */
    public void sendFriendRequest(Long id1,Long id2){//TODO check if frq exists
        FriendRequest frq=new FriendRequest(new Tuple<Long,Long>(id1,id2),FRQStatus.PENDING);
        frqRepo.save(frq);
    }
    public void rejectFriendRequest(Long id1,Long id2){//TODO if frq gets rejected and id is <u1,u2> then what?
        FriendRequest frq=frqRepo.findOne(new Tuple<Long,Long>(id1,id2));
        frq.setStatus(FRQStatus.REJECTED);
        frqRepo.update(frq);
    }
    public void acceptFriendRequest(Long id1,Long id2)throws PrietenieException{
        FriendRequest frq=frqRepo.findOne(new Tuple<Long,Long>(id1,id2));
        frq.setStatus(FRQStatus.ACCEPTED);
        frqRepo.update(frq);
        addPrietenie(id1,id2);
    }
    /**
     * Creates and stores a Prietenie entity
     * @param id1 - Long
     * @param id2 - Long
     * @return null if the entity is saved,
     *                          the entity otherwise
     */
    public Prietenie addPrietenie(Long id1,Long id2) throws PrietenieException{
        Prietenie p=new Prietenie(id1,id2);
        Utilizator u1= utiRepo.findOne(id1);
        Utilizator u2= utiRepo.findOne(id2);
        if(u1==null || u2==null)
            throw new PrietenieException("Unul sau ambii utilizatori nu exista!");
        u1.addFriend(u2);
        u2.addFriend(u1);
        Prietenie ret = repo.save(p);
        return ret;
    }

    /**
     * Deletes the Prietenie entity with the given id
     * @param id - Long
     * @return the removed entity, or null if there was none with the given id
     */
    public Prietenie deletePrietenie(Tuple<Long,Long> id)throws NuExistaInRepoException{

        Prietenie pr=repo.findOne(id);
        if(pr==null)
            throw new NuExistaInRepoException("Cei doi useri nu sunt prieteni!");
        Utilizator u1=utiRepo.findOne(pr.getId().getRight());
        Utilizator u2=utiRepo.findOne(pr.getId().getLeft());
        if(u1!=null && u2!=null){
            u1.removeFriend(u2);
            u2.removeFriend(u1);
            return repo.delete(id);
        }
        return null;
    }

    public Prietenie getOne(Tuple<Long,Long> id){
        return repo.findOne(id);
    }
    /**
     * Deletes the user with the given id and the friendships he is part of
     * @param id - Long
     * @return the removed entity, or null if there was none with the given id
     */
    public Utilizator deleteUtilizatorAndFriendships(Long id)throws NuExistaInRepoException{
        Utilizator u=utiRepo.findOne(id);
        if(u!=null){
            List<Utilizator> fl=new ArrayList<>();
            for(Utilizator ut:u.getFriends())
                fl.add(ut);
           for(Utilizator uti:fl){
               deletePrietenie(new Tuple<Long,Long>(uti.getId(),u.getId()));
           }
        }
        return utiRepo.delete(id);
    }
    public Iterable<Prietenie> getAll(){
        return repo.findAll();
    }


    private int nrCompConexe=0;


    private Map<Integer,Integer> maxPath=new HashMap<>();
    private Map<Integer,Integer> currentPath=new HashMap<>();
    /**
     * Marks the vizitat list of nodes with the corresponding conex component number
     * @param x - the starting node
     * @param graf - an disoriented graph
     * @param vizitat
     */
    void dfs(int x, List<Integer[]> graf, int[] vizitat){
        if(graf.get(x)==null)//daca nodul curent nu are vecini
            return;
        int cc;
        if(vizitat[x]==0)//daca nu e marcat pana acum => face parte dintr-o noua componenta conexa
            cc=nrCompConexe++;
        else
            cc=vizitat[x];
        vizitat[x]=cc;
        for(int f:graf.get(x)){//pentru toti vecinii nodului curent
            if(vizitat[f]==0){
                vizitat[f]=cc;
                dfs(f,graf,vizitat);
            }
        }
    }

    /**
     *
     * @return o lista de liste, pe fiecare pozitie x fiind lista cu id-urile prietenilor lui x
     */
    private List<Integer[]> getGraf(){
        int lst=Integer.parseInt(utiRepo.getLastID());
        List<Integer[]> graf=new ArrayList<>();
        for(int i=0;i<=lst;i++)
        {
            Utilizator u=utiRepo.findOne(Long.valueOf((long)i));
            if(u!=null) {
                List<Utilizator> fl = u.getFriends();
                List<Integer> frl = new ArrayList<>();
                fl.forEach(x -> frl.add(x.getId().intValue()));
                Integer[] intfrl=new Integer[frl.size()];
                intfrl=frl.toArray(intfrl);
                graf.add(intfrl);
            }else{
                graf.add(null);
            }
        }
        return graf;
    }


    /**
     *
     * @return lista de liste de id-uri, fiecare sublista reprezentand o componenta conexa
     */
    public List<List<Integer>> getComponenteConexe(){
        nrCompConexe=0;
        //iau ultimul id, ca sa stiu cati utilizatori am
        int lstId=Integer.parseInt(utiRepo.getLastID());
        //fac un vector de utilizatori vizitati pentru dfs
        int[] vizitat=new int[lstId+1];
        //componentele conexe vor fi returnate sub forma de lista de liste
        List<List<Integer>> compConexe=new ArrayList<>();

        //fac un graf (liste de adiacenta) din prietenii fiecarui utilizator
        List<Integer[]> graf=getGraf();
        for(int i=0;i<=lstId;i++)//pentru fiecare id posibil
        {
            if(currentPath.size()>maxPath.size())
                maxPath=currentPath;
            currentPath=new HashMap<>();
            dfs(i,graf,vizitat);
        }

        //creez o lista de liste goale
        for(int i=0;i<nrCompConexe;i++){
            List<Integer> compI=new ArrayList<>();
            compConexe.add(compI);
        }
        //stabilesc componentele conexe
        for(int i=0;i<=lstId;i++){//pentru fiecare id posibil
            if(utiRepo.findOne(Long.valueOf(i))!=null){//daca exista utilizatorul cu id-ul i
                List<Integer> l= compConexe.get(vizitat[i]);//obtin lista corespunzatoare id-ului
                l.add(i);//il pun in lista
            }
        }
        compConexe.removeIf(x->{
            return x.size()<1;
        });

        return compConexe;
    }

    public List<Integer> getMostSociableCommunity(){
        List<Integer> lst=new ArrayList<>();
        List<List<Integer>> cc=getComponenteConexe();
        int lmax=0;
        for(List<Integer> l:cc)
            if(l.size()>lmax){
                lmax=l.size();
                lst=l;
            }
        return lst;
    }
    ///TO DO: add other methods
}
