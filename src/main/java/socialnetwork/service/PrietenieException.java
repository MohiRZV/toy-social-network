package socialnetwork.service;

public class PrietenieException extends Exception{
    public PrietenieException() {
    }

    public PrietenieException(String message) {
        super(message);
    }

    public PrietenieException(String message, Throwable cause) {
        super(message, cause);
    }

    public PrietenieException(Throwable cause) {
        super(cause);
    }

    public PrietenieException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
