//package socialnetwork.repository.file;
//
//import socialnetwork.domain.Entity;
//import socialnetwork.domain.validators.Validator;
//import socialnetwork.repository.memory.InMemoryRepository;
//
//import java.io.*;
//
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.util.Arrays;
//import java.util.List;
//
//
/////Aceasta clasa implementeaza sablonul de proiectare Template Method; puteti inlucui solutia propusa cu un Factori (vezi mai jos)
//public abstract class AbstractFileRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID,E> {
//    String fileName;
//    private String lastID;
//    public AbstractFileRepository(String fileName, Validator<E> validator) {
//        super(validator);
//        this.fileName=fileName;
//        lastID="0";
//        loadData();
//
//    }
//
//    /**
//     * Loads all the data from the given file into memory
//     */
//    private void loadData() {
//
//        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
//            String linie;
//            while((linie=br.readLine())!=null){
//                List<String> attr=Arrays.asList(linie.split(";"));
//                E e=extractEntity(attr);
//                lastID=e.getId().toString();
//                super.save(e);
//            }
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//
//        }
//
//        //sau cu lambda - curs 4, sem 4 si 5
////        Path path = Paths.get(fileName);
////        try {
////            List<String> lines = Files.readAllLines(path);
////            lines.forEach(linie -> {
////                E entity=extractEntity(Arrays.asList(linie.split(";")));
////                super.save(entity);
////            });
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
//
//    }
//
//    /**
//     *  extract entity  - template method design pattern
//     *  creates an entity of type E having a specified list of @code attributes
//     * @param attributes
//     * @return an entity of type E
//     */
//    public abstract E extractEntity(List<String> attributes);
//    ///Observatie-Sugestie: in locul metodei template extractEntity, puteti avea un factory pr crearea instantelor entity
//
//    /**
//     * creates an entity as a string to be saved
//     * @param entity
//     * @return a string corresponding to the data to be stored from a given entitiy
//     */
//    protected abstract String createEntityAsString(E entity);
//
//    @Override
//    public E save(E entity){
//        E e=super.save(entity);
//        if (e==null)
//        {
//            writeToFile(entity);
//        }
//        return e;
//
//    }
//
//    /**
//     * Writes the given entity in file
//     * @param entity
//     */
//    protected void writeToFile(E entity){
//        try (BufferedWriter bW = new BufferedWriter(new FileWriter(fileName,true))) {
//            bW.write(createEntityAsString(entity));
//            bW.newLine();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    @Override
//    public String getLastID(){
//        return lastID;
//    }
//
//    @Override
//    public void setLastID(String lastID){this.lastID=lastID;}
//
//    public void writeAll(){
//        try {
//            BufferedWriter bW = new BufferedWriter(new FileWriter(fileName,false));
//            bW.close();
//            findAll().forEach(x->writeToFile(x));
//        }catch(IOException ioException){
//            System.out.println(ioException.getMessage());
//        }
//    }
//
//    @Override
//    public E delete(ID id){
//        E ret = super.delete(id);
//        if(ret!=null){
//            writeAll();
//        }
//        return ret;
//    }
//}
//
