//package socialnetwork.repository.file;
//
//import socialnetwork.domain.FRQStatus;
//import socialnetwork.domain.FriendRequest;
//import socialnetwork.domain.Tuple;
//import socialnetwork.domain.validators.Validator;
//
//import java.util.List;
//
//public class FriendRequestFile extends AbstractFileRepository<Tuple<Long,Long>, FriendRequest>{
//
//    public FriendRequestFile(String fileName, Validator<FriendRequest> validator) {
//        super(fileName, validator);
//    }
//
//    @Override
//    public FriendRequest extractEntity(List<String> attributes) {
//        String[] id=attributes.get(0).split(",");
//        Tuple<Long,Long> ID=new Tuple<>(Long.parseLong(id[0]),Long.parseLong(id[1]));
//        String status=attributes.get(1);
//        return new FriendRequest(ID, FRQStatus.getBySymbol(status));
//    }
//
//    @Override
//    protected String createEntityAsString(FriendRequest entity) {
//        String id=entity.getId().getLeft().toString()+','+entity.getId().getRight();
//        String status=entity.getStatus().name();
//        return id+';'+status;
//    }
//}
