package socialnetwork.repository;

public class NuExistaInRepoException extends Exception{
    public NuExistaInRepoException(String message) {
        super(message);
    }
}
