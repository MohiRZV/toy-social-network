package socialnetwork.repository.database;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UtilizatorDB extends AbstractDBRepository<Long, Utilizator>{
    public UtilizatorDB(String url, String username, String password, Validator<Utilizator> validator, String tableName,String columns,String serialSeqName) {
        super(url, username, password, validator, tableName, columns,serialSeqName);
    }

    @Override
    protected Utilizator extractEntity(ResultSet resultSet) {
        Utilizator u=null;
        try{
            Long id = resultSet.getLong("id");
            String firstName = resultSet.getString("firstname");
            String lastName = resultSet.getString("lastname");

            u = new Utilizator(firstName, lastName);
            u.setId(id);
            return u;
        }catch(SQLException sqlException){
            sqlException.printStackTrace();
        }
        return u;
    }

    @Override
    protected String getEntityAsString(Utilizator entity) {
        return "('"+entity.getFirstName()+"','"+entity.getLastName()+"')";
    }
    @Override
    public String getSetSequence(Utilizator entity) {
        return null;
    }

}
