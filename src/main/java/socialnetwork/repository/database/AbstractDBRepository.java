package socialnetwork.repository.database;

import socialnetwork.domain.Entity;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class AbstractDBRepository<ID, E extends Entity<ID>> implements Repository<ID,E> {
    protected String url;
    protected String username;
    protected String password;
    protected String tableName;
    protected String columns;
    private Validator<E> validator;
    private String serialSeqName;


    public AbstractDBRepository(String url, String username, String password, Validator<E> validator,String tableName, String columns,String serialSeqName) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
        this.tableName=tableName;
        this.columns=columns;
        this.serialSeqName=serialSeqName;
    }

    @Override
    public E findOne(ID id) {
        String query="SELECT * from "+tableName+" where id="+id.toString();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {
            E entity=null;
            while(resultSet.next())
                 entity=extractEntity(resultSet);
            return entity;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected abstract E extractEntity(ResultSet resultSet);

    protected abstract String getEntityAsString(E entity);
    @Override
    public Iterable<E> findAll() {
        Set<E> entities = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from "+tableName);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                E entity=extractEntity(resultSet);
                entities.add(entity);
            }
            return entities;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entities;
    }

    @Override
    public E save(E entity) {
        String query="insert into "+tableName+" "+columns+" values "+" "+getEntityAsString(entity);
        try (Connection connection = DriverManager.getConnection(url, username, password);
             Statement statement = connection.createStatement();) {
            statement.executeUpdate(query);
            return entity;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public E delete(ID id) {
        E entity=findOne(id);
        if(entity!=null){
            String query="delete from "+tableName+" where id="+id.toString();
            try (Connection connection = DriverManager.getConnection(url, username, password);
                 Statement statement = connection.createStatement();) {
                statement.executeUpdate(query);
                return entity;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public abstract String getSetSequence(E entity);
    @Override
    public E update(E entity) {
        if(entity!=null){
            String query="UPDATE "+tableName+" SET "+getSetSequence(entity)+" WHERE id="+entity.getId();
            try (Connection connection = DriverManager.getConnection(url, username, password);
                 Statement statement = connection.createStatement();) {
                statement.executeUpdate(query);
                return entity;
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public String getLastID() {
        String query="SELECT last_value from "+serialSeqName;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery();) {
            String r="";
            while(resultSet.next())
                r=resultSet.getString(1);
            return r;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }


}
