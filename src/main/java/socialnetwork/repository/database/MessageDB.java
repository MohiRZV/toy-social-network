package socialnetwork.repository.database;

import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MessageDB extends AbstractDBRepository<Long, Message> {
    private Repository<Long,Utilizator> utiRepo;
    public MessageDB(String url, String username, String password, Validator<Message> validator, String tableName, String columns,String serialSeqName,Repository<Long,Utilizator> utiRepo) {
        super(url, username, password, validator, tableName, columns,serialSeqName);
        this.utiRepo=utiRepo;
    }

    @Override
    protected Message extractEntity(ResultSet resultSet) {
        //create table messages (id BIGSERIAL PRIMARY KEY,from_id BIGINT,to_ids varchar(300),message varchar(300),data varchar(100),reply varchar(300));
        Message msg = null;
        try {
            Long id = resultSet.getLong("id");
            Long from = resultSet.getLong("from_id");
            String[] toS = resultSet.getString("to_ids").split(";");
            List<Long> to = new ArrayList<>();
            for (String i : toS)
                to.add(Long.parseLong(i));
            String message = resultSet.getString("message");
            String dataS = resultSet.getString("data");
            LocalDateTime data = LocalDateTime.parse(dataS);
            String replyS = resultSet.getString("reply");

//            msg = new Message(id, from, to, message);
//            msg.setDate(data);
//            if (replyS.length()>0) {
//                String[] replyIds = replyS.split(",");
//                for (String i : replyIds)
//                    msg.addReply(Long.parseLong(i));
            List<Utilizator> toU=new ArrayList<>();
            for(Long i:to)
                toU.add(utiRepo.findOne(i));
            msg = new Message(id, from, toU, message);
            msg.setDate(data);
            if (replyS.length()>0) {
                String[] replyIds = replyS.split(";");
                for (String i : replyIds)
                    msg.addReply(findOne(Long.parseLong(i)));
            }
                return msg;
            }catch(SQLException sqlException){
                sqlException.printStackTrace();
        }
        return msg;
    }


    @Override
    protected String getEntityAsString(Message entity) {
//        String from=entity.getFrom().toString();
//        List<Long> Lto=entity.getTo();
//        String to=Lto.get(0).toString();
//        for(int index=1;index<Lto.size();index++)
//            to=to+";"+Lto.get(index).toString();
//        String message=entity.getMessage();
//        String date=entity.getData().toString();
//        String reply="";
//        List<Long> repl=entity.getReply();
//        if(repl.size()>0) {
//            reply = repl.get(0).toString();
//            for(int index=1;index<repl.size();index++)
//                reply = reply + ';' + repl.get(index).toString();
//        }
//        return "("+from+','+"'"+to+"'"+','+"'"+message+"'"+','+"'"+date+"'"+','+"'"+reply+"')";
        String from=entity.getFrom().toString();
        List<Utilizator> lu=entity.getTo();
        List<Long> Lto=new ArrayList<>();
        for(Utilizator u:lu)
            Lto.add(u.getId());
        String to=Lto.get(0).toString();
        for(int index=1;index<Lto.size();index++)
            to=to+";"+Lto.get(index).toString();
        String message=entity.getMessage();
        String date=entity.getData().toString();
        String reply="";
        List<Message> lm=entity.getReply();
        List<Long> repl=new ArrayList<>();
        for(Message m:lm)
            repl.add(m.getId());
        if(repl.size()>0) {
            reply = repl.get(0).toString();
            for(int index=1;index<repl.size();index++)
                reply = reply + ';' + repl.get(index).toString();
        }
        return "("+from+','+"'"+to+"'"+','+"'"+message+"'"+','+"'"+date+"'"+','+"'"+reply+"')";
    }

    @Override
    public String getSetSequence(Message entity) {
        List<Message> r=entity.getReply();
        List<Long> repl=new ArrayList<>();
        for(Message m:r)
            repl.add(m.getId());
        String reply = repl.get(0).toString();
        for(int index=1;index<repl.size();index++)
            reply = reply + ';' + repl.get(index).toString();
        return "message='"+entity.getMessage()+"',reply='"+reply+"'";
    }


}
