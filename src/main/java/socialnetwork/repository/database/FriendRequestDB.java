package socialnetwork.repository.database;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.Validator;

import java.sql.*;
import java.time.LocalDateTime;

public class FriendRequestDB extends AbstractDBRepository<Tuple<Long,Long>, FriendRequest> {
    public FriendRequestDB(String url, String username, String password, Validator<FriendRequest> validator, String tableName, String columns) {
        super(url, username, password, validator, tableName, columns,"");
    }

    @Override
    protected FriendRequest extractEntity(ResultSet resultSet) {
        try {
            Long id1=resultSet.getLong("id1");
            Long id2=resultSet.getLong("id2");
            Tuple<Long,Long> id=new Tuple<>(id1,id2);
            String status=resultSet.getString("status");
            FriendRequest p=new FriendRequest(id, FRQStatus.getBySymbol(status));
            return p;
        }catch(SQLException sqlException){
            sqlException.printStackTrace();
        }
        return null;
    }

    @Override
    protected String getEntityAsString(FriendRequest entity) {
        String id1=entity.getId().getLeft().toString();
        String id2=entity.getId().getRight().toString();
        String status=entity.getStatus().name();
        return "("+id1+","+id2+",'"+status+"')";
    }

    @Override
    public String getSetSequence(FriendRequest frq) {
        return "status='"+frq.getStatus().name()+"'";
    }

    @Override
    public FriendRequest findOne(Tuple<Long, Long> longLongTuple) {

        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("select * from "+tableName+" where id1=? and id2=?");
            statement.setLong(1,longLongTuple.getLeft());
            statement.setLong(2,longLongTuple.getRight());
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            FriendRequest entity=null;
            while(resultSet.next())
                entity=extractEntity(resultSet);
            return entity;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FriendRequest update(FriendRequest entity) {
        if(entity!=null){
            String query="UPDATE "+tableName+" SET "+getSetSequence(entity)+" WHERE id1="+entity.getId().getLeft().toString()+" and id2="+entity.getId().getRight();
            try (Connection connection = DriverManager.getConnection(url, username, password);
                 Statement statement = connection.createStatement();) {
                statement.executeUpdate(query);
                return entity;
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }
}
