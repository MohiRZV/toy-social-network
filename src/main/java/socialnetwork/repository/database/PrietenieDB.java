package socialnetwork.repository.database;

import jdk.vm.ci.meta.Local;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

//create table prietenii(id varchar(50),date varchar(50));
public class PrietenieDB extends AbstractDBRepository<Tuple<Long,Long>, Prietenie> {
    public PrietenieDB(String url, String username, String password, Validator<Prietenie> validator, String tableName, String columns) {
        super(url, username, password, validator, tableName, columns,"");
    }

    @Override
    protected Prietenie extractEntity(ResultSet resultSet) {
        try {
            Long id1 = resultSet.getLong("id1");
            Long id2 = resultSet.getLong("id2");
            LocalDateTime data= LocalDateTime.parse(resultSet.getString("date"));
            Prietenie p=new Prietenie(id1,id2);
            p.setDate(data);
            return p;
        }catch(SQLException sqlException){
            sqlException.printStackTrace();
        }
        return null;
    }

    @Override
    protected String getEntityAsString(Prietenie entity) {
        String id1=entity.getId().getLeft().toString();
        String id2=entity.getId().getRight().toString();
        String date=entity.getDate().toString();
        return "("+id1+","+id2+",'"+date+"')";
    }

    @Override
    public String getSetSequence(Prietenie entity) {
        return null;
    }

    @Override
    public Prietenie findOne(Tuple<Long, Long> longLongTuple) {

        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("select * from "+tableName+" where id1=? and id2=?");
            statement.setLong(1,longLongTuple.getLeft());
            statement.setLong(2,longLongTuple.getRight());
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            Prietenie entity=null;
            while(resultSet.next())
                entity=extractEntity(resultSet);
            return entity;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
