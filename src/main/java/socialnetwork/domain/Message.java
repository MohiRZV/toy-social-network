package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Message extends Entity<Long>{
    Long from;
    List<Utilizator> to;
    String message;
    LocalDateTime data;
    List<Message> reply;

    public Message(Long id, Long from, List<Utilizator> to, String message) {
        setId(id);
        this.from = from;
        this.to = to;
        this.message = message;
        data=LocalDateTime.now();
        reply=new ArrayList<>();
    }

    public Long getFrom() {
        return from;
    }

    public List<Utilizator> getTo() {
        return to;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getData() {
        return data;
    }

    public List<Message> getReply() {
        return reply;
    }

    public void addReply(Message msg) {
        reply.add(msg);
    }
    public void setDate(LocalDateTime date){data=date;}

    @Override
    public String toString() {
        return "Message{" +
                "from=" + from +
                ", message='" + message + '\'' +
                ", to= "+to+
                ", data=" + data +
                ", reply=" + reply +
                '}';
    }
}
