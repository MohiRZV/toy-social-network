package socialnetwork.domain;

import java.time.LocalDateTime;

//create table prietenii(id varchar(50),date varchar(50));
public class Prietenie extends Entity<Tuple<Long,Long>> {

    LocalDateTime date;

    public Prietenie(Long p1,Long p2) {
        setId(new Tuple<Long,Long>(p1,p2));
        date=LocalDateTime.now();
    }

    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate() {
        return date;
    }
    public void setDate(LocalDateTime d){date=d;}
    @Override
    public String toString() {
        return getId().getLeft().toString()+" + "+getId().getRight()+" = lOve";
    }
}
