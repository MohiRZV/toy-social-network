package socialnetwork.domain.validators;

import socialnetwork.domain.Utilizator;

public class UtilizatorValidator implements Validator<Utilizator> {
    @Override
    public void validate(Utilizator entity) throws ValidationException {
        if(entity.getFirstName().equals(""))
            throw new ValidationException("First Name cannot be null!");
        if(entity.getLastName().equals(""))
            throw new ValidationException("Last Name cannot be null!");
    }
}
