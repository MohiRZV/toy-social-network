package socialnetwork.domain.validators;

import socialnetwork.domain.Message;

public class MessageValidator implements Validator<Message>{
    @Override
    public void validate(Message entity) throws ValidationException {
        if(entity.getTo()==null)
            throw new ValidationException("Emitatorul nu poate fi nimeni!");
        if(entity.getFrom()==null)
            throw new ValidationException("Receptorul nu poate fi nimeni!");
        if(entity.getMessage()==null)
            throw new ValidationException("Mesajul nu poate fi nul!");
    }
}
