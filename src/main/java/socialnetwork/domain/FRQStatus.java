package socialnetwork.domain;

public enum FRQStatus {
    ACCEPTED("ACCEPTED"),
    REJECTED("REJECTED"),
    PENDING("PENDING");
    private String symbol;

    FRQStatus(String symbol) {
        this.symbol=symbol;
    }
    public static FRQStatus getBySymbol(String symbol){
        for(FRQStatus frqs:FRQStatus.values()){
            if(frqs.symbol.equals(symbol))
                return frqs;
        }
        return null;
    }
}
