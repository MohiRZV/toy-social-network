package socialnetwork.domain;
//create table friendrequests (id varchar(100) primary key,status varchar(30));
public class FriendRequest extends Entity<Tuple<Long,Long>>{
    private FRQStatus status;

    public FriendRequest(Tuple<Long,Long> id,FRQStatus status) {
        setId(id);
        this.status = status;
    }

    public FRQStatus getStatus() {
        return status;
    }

    public void setStatus(FRQStatus status) {
        this.status = status;
    }
}
